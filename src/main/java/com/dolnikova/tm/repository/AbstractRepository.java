package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final Map<String, E> map = new LinkedHashMap<>();

    public AbstractRepository() {
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> all = new ArrayList<>();
        for (Map.Entry<String, E> entity : map.entrySet()) {
            if (entity.getValue().getUserId().equals(userId)) all.add(entity.getValue());
        }
        return all;
    }

    @Override
    public E findOne(final String userId, final String id) {
        for (Map.Entry<String, E> entity : map.entrySet()) {
            final E foundEntity = entity.getValue();
            if (id.equals(foundEntity.getId())
                    && entity.getValue().getUserId().equals(userId))
                return foundEntity;
        }
        return null;
    }

    public E findOne(final String login) {
        return null;
    }

    @Override
    public void persist(final String userId, final E entity) {
        if (entity.getUserId().equals(userId)) {
            map.put(entity.getId(), entity);
        }
    }

    @Override
    public void merge(final String userId, final String name, final E entity) {
        if (entity.getUserId().equals(userId)) {
            entity.setName(name);
        }
    }

    public void merge(final String newData, final String userId, final User user, final DataType dataType) {

    }

    @Override
    public void remove(final String userId, final String id) {
        final E entityToRemove = map.get(id);
        if (entityToRemove.getUserId().equals(userId)) map.remove(id);
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (entity.getUserId().equals(userId)) map.remove(entity.getId());
    }

    @Override
    public void removeAll(final String userId) {
        for (Iterator<Map.Entry<String, E>> it = map.entrySet().iterator(); it.hasNext(); ) {
            final Map.Entry<String, E> entry = it.next();
            if (entry.getValue().getUserId().equals(userId)) {
                it.remove();
            }
        }
    }

    public List<Task> getTasksByProjectId(final String userId, final String projectId) {
        return null;
    }
}
