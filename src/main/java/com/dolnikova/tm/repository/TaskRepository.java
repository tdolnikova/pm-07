package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

    @Override
    public List<Task> getTasksByProjectId(final String userId, final String projectId) {
        final List<Task> allTasks = findAll(userId);
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : allTasks) {
            if (task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

}
