package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;

public class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return Constant.REMOVE_PROJECT;
    }

    @Override
    public String description() {
        return Constant.REMOVE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        if (serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return;
        }
        System.out.println(Constant.WHICH_PROJECT_DELETE);
        Project project = null;
        while (project == null) {
            final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) return;
            project = serviceLocator.getProjectService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (project != null) serviceLocator.getProjectService().remove(serviceLocator.getUserService().getCurrentUser().getId(), project);
            else System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        System.out.println(Constant.PROJECT_DELETED);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
