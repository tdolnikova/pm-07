package com.dolnikova.tm.command.project;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;

public class ProjectRemoveAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return Constant.REMOVE_ALL_PROJECTS;
    }

    @Override
    public String description() {
        return Constant.REMOVE_ALL_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        serviceLocator.getProjectService().removeAll(serviceLocator.getUserService().getCurrentUser().getId());
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
