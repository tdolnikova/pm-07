package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;

public class UserSignOutCommand extends AbstractCommand {

    @Override
    public String command() {
        return Constant.USER_SIGN_OUT;
    }

    @Override
    public String description() {
        return Constant.USER_SIGN_OUT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        serviceLocator.getUserService().setCurrentUser(null);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
