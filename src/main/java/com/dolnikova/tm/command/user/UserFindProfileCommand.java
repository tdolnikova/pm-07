package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;

public class UserFindProfileCommand extends AbstractCommand {

    @Override
    public String command() {
        return Constant.USER_FIND_PROFILE;
    }

    @Override
    public String description() {
        return Constant.USER_FIND_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("id: " + serviceLocator.getUserService().getCurrentUser().getId());
        System.out.println("login: " + serviceLocator.getUserService().getCurrentUser().getLogin());
        System.out.println("password: " + serviceLocator.getUserService().getCurrentUser().getPassword());
        System.out.println("role: " + serviceLocator.getUserService().getCurrentUser().getRole());
    }

    @Override
    public boolean isSecure() {
        return (!serviceLocator.getUserService().getCurrentUser().getId().isEmpty());
    }

}
