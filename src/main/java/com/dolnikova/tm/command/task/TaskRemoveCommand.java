package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

import java.util.List;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return Constant.REMOVE_TASK;
    }

    @Override
    public String description() {
        return Constant.REMOVE_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        final Project project = findProject();
        if (project == null) return;
        final List<Task> projectTasks = serviceLocator.getTaskService().getTasksByProjectId(serviceLocator.getUserService().getCurrentUser().getId(), project.getId());
        int taskSize = projectTasks.size();
        if (taskSize == 0) {
            System.out.println(Constant.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(Constant.IN_PROJECT + project.getName() + " " + taskSize + " " + Constant.OF_TASKS_WITH_POINT + ":");
        for (final Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Constant.INSERT_TASK_ID);
        boolean taskDeleted = false;
        while (!taskDeleted) {
            final String taskIdToDelete = Bootstrap.scanner.nextLine();
            if (taskIdToDelete.isEmpty()) break;
            final Task taskToRemove = serviceLocator.getTaskService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), taskIdToDelete);
            serviceLocator.getTaskService().remove(serviceLocator.getUserService().getCurrentUser().getId(), taskToRemove);
            System.out.println(Constant.TASK_DELETED);
            taskDeleted = true;
        }
    }

    public Project findProject() {
        if (serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_TASKS);
            return null;
        }
        if (serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        Project project = null;
        while (project == null) {
            final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }

}
