package com.dolnikova.tm.command.about;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.jcabi.manifests.Manifests;

public class AboutCommand extends AbstractCommand {

    @Override
    public String command() {
        return Constant.ABOUT;
    }

    @Override
    public String description() {
        return Constant.ABOUT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task Manager ver.SE-07, " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));

    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
