package com.dolnikova.tm.command;

import com.dolnikova.tm.api.bootstrap.ServiceLocator;

public abstract class AbstractCommand {
    public ServiceLocator serviceLocator;
    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    public abstract String command();
    public abstract String description();
    public abstract void execute() throws Exception;
    public abstract boolean isSecure();
}