package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Override
    List<User> findAll(final String userId);

    @Override
    User findOne(final String userId, final String id);

    User findOne(final String login);

    @Override
    void persist(final String userId, final User entity);

    @Override
    void merge(final String userId, final String newName, final User entity);

    void merge(final String newData, final String userId, final User user, final DataType dataType);

    @Override
    void remove(final String userId, final String id);

    @Override
    void remove(final String userId, final User entity);

    @Override
    void removeAll(final String userId);

}
