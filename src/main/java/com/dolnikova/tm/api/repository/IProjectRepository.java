package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Override
    List<Project> findAll(final String userId);

    @Override
    Project findOne(final String userId, final String id);

    @Override
    void persist(final String userId, final Project entity);

    @Override
    void merge(final String userId, final String newName, final Project entity);

    @Override
    void remove(final String userId, final String id);

    @Override
    void remove(final String userId, final Project entity);

    @Override
    void removeAll(final String userId);
}
