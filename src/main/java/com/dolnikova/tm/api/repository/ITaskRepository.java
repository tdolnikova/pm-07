package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Override
    List<Task> findAll(final String userId);

    @Override
    Task findOne(final String userId, final String id);

    @Override
    void persist(final String userId, final Task entity);

    @Override
    void merge(final String userId, final String newName, final Task entity);

    @Override
    void remove(final String userId, final String id);

    @Override
    void remove(final String userId, final Task entity);

    @Override
    void removeAll(final String userId);

    List<Task> getTasksByProjectId(final String userId, final String projectId);
}
