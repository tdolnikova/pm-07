package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {
    List<E> findAll(final String userId);
    E findOne(final String userId, final String id);
    void persist(final String userId, final E entity);
    void merge(final String userId, final String newName, final E entity);
    void remove(final String userId, final String id);
    void remove(final String userId, final E entity);
    void removeAll(final String userId);
}
