package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;

import java.util.List;

public interface IUserService extends IService<User> {

    @Override
    List<User> findAll(final String userId);

    @Override
    User findOne(final String userId, final String id);

    @Override
    void persist(final String userId, final User entity);

    @Override
    void merge(final String userId, final String newName, final User entityToMerge);

    @Override
    void remove(final String userId, final User entity);

    @Override
    void removeAll(final String userId);

    User getCurrentUser();

    void setCurrentUser(final User currentUser);

    User findOne(final String login);

    void merge(final String newData, final String userId, final User user, final DataType dataType);

}
