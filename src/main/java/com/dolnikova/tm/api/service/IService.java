package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {
    List<E> findAll(final String userId);
    E findOne(final String userId, final String id);
    void persist(final String userId, final E entity);
    void merge(final String userId, final String newName, final E entityToMerge);
    void remove(final String userId, final E entity);
    void removeAll(final String userId);
}
