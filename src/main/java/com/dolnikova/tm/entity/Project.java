package com.dolnikova.tm.entity;

import java.util.Date;
import java.util.UUID;

public class Project extends AbstractEntity {

    private String id;
    private String userId;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;

    public Project() {}

    public Project(final String userId, final String projectName) {
        this.name = projectName;
        this.userId = userId;
        id = UUID.randomUUID().toString();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
