package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.util.HashUtil;

import java.util.UUID;

public class User extends AbstractEntity {

    private String id = "";
    private String userId = "";
    private String login = "";
    private String password;
    private Role role;

    public User(final String login, final String password, final Role role) {
        this.id = UUID.randomUUID().toString();
        userId = this.id;
        this.login = login;
        this.password = HashUtil.stringToHashString(password);
        this.role = role;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }
}
