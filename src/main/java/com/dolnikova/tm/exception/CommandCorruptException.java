package com.dolnikova.tm.exception;

public class CommandCorruptException extends Exception {

    public CommandCorruptException() {
        super("Command doesn't exist");
    }

}
