package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.api.bootstrap.ServiceLocator;
import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.command.about.AboutCommand;
import com.dolnikova.tm.command.exit.ExitCommand;
import com.dolnikova.tm.command.help.HelpCommand;
import com.dolnikova.tm.command.project.*;
import com.dolnikova.tm.command.task.*;
import com.dolnikova.tm.command.user.*;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.exception.CommandCorruptException;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.repository.UserRepository;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.TaskService;
import com.dolnikova.tm.service.UserService;

import java.util.*;

public class Bootstrap implements ServiceLocator {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public final static Scanner scanner = new Scanner(System.in);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);

    public void init() {
        final User user = new User("user", "user", Role.USER);
        registryUser(user);
        userService.setCurrentUser(user);
        registryUser(new User("admin", "admin", Role.ADMIN));

        try {
            registryCommand(new HelpCommand());
            registryCommand(new ExitCommand());
            registryCommand(new AboutCommand());

            registryCommand(new UserAuthCommand());
            registryCommand(new UserChangePasswordCommand());
            registryCommand(new UserEditProfileCommand());
            registryCommand(new UserFindProfileCommand());
            registryCommand(new UserRegisterCommand());
            registryCommand(new UserSignOutCommand());

            registryCommand(new ProjectFindAllCommand());
            registryCommand(new ProjectFindOneCommand());
            registryCommand(new ProjectMergeCommand());
            registryCommand(new ProjectPersistCommand());
            registryCommand(new ProjectRemoveAllCommand());
            registryCommand(new ProjectRemoveCommand());

            registryCommand(new TaskFindAllCommand());
            registryCommand(new TaskFindOneCommand());
            registryCommand(new TaskMergeCommand());
            registryCommand(new TaskPersistCommand());
            registryCommand(new TaskRemoveAllCommand());
            registryCommand(new TaskRemoveCommand());

            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registryUser(final User user) {
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        if (user.getRole() == null) return;
        userService.persist(user.getId(), user);
    }

    public void registryCommand(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

}
