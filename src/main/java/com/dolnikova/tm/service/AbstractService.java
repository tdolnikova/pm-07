package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import com.dolnikova.tm.repository.AbstractRepository;

import java.util.List;

public abstract class AbstractService <E extends AbstractEntity> implements IService<E> {

    private IRepository<E> abstractRepository;

    AbstractService() {
    }

    AbstractService(final IRepository<E> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId.isEmpty()) return null;
        return findAll(userId);
    }

    @Override
    public E findOne(final String userId, final String id) {
        if (userId.isEmpty() || abstractRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return abstractRepository.findOne(userId, id);
    }

    public E findOne(final String login) {
        return null;
    }

    @Override
    public void persist(final String userId, final E entity) {
        if (userId.isEmpty() || entity == null) return;
        abstractRepository.persist(userId, entity);
    }

    @Override
    public void merge(final String userId, final String newName, final E entityToMerge) {
        if (userId.isEmpty() || newName.isEmpty() || entityToMerge == null) return;
        abstractRepository.merge(userId, newName, entityToMerge);
    }

    public void merge(final String newData, final String userId, final User user, final DataType dataType) {

    }

    @Override
    public void remove(final String userId, final E entity) {
        if (userId.isEmpty() || entity == null) return;
        abstractRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(final String userId) {
        if (userId.isEmpty()) return;
        abstractRepository.removeAll(userId);
    }

}
