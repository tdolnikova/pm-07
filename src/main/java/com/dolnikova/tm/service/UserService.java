package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import com.dolnikova.tm.repository.AbstractRepository;

import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository userRepository;
    private User currentUser = null;

    public UserService() {
    }

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll(final String userId) {
        if (userId.isEmpty()) return null;
        return userRepository.findAll(userId);
    }

    @Override
    public User findOne(final String userId, final String id) {
        if (userId.isEmpty() || userRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return userRepository.findOne(userId, id);
    }

    @Override
    public void persist(final String userId, final User entity) {
        if (userId.isEmpty() || entity == null) return;
        userRepository.persist(userId, entity);
    }

    @Override
    public void remove(final String userId, final User entity) {
        if (userId.isEmpty() || entity == null) return;
        userRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(final String userId) {
        if (userId.isEmpty()) return;
        userRepository.removeAll(userId);
    }

    @Override
    public User findOne(final String login) {
        if (login.isEmpty()) return null;
        return userRepository.findOne(login);
    }

    @Override
    public void merge(final String newPassword, final String userId, final User user) {
        if (newPassword.isEmpty() || userId.isEmpty() || user == null) return;
        userRepository.merge(newPassword, userId, user);
    }

    @Override
    public void merge(final String newData, final String userId, final User user, final DataType dataType) {
        if (newData.isEmpty() || userId.isEmpty() || user == null || dataType == null) return;
        userRepository.merge(newData, userId, user, dataType);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }
}
