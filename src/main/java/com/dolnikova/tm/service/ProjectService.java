package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.repository.AbstractRepository;

import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository projectRepository;
    public ProjectService() {
    }

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

    @Override
    public Project findOne(final String userId, final String id) {
        if (userId.isEmpty() || projectRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return projectRepository.findOne(userId, id);
    }

    @Override
    public void persist(final String userId, final Project entity) {
        if (userId.isEmpty() || entity == null) return;
        projectRepository.persist(userId, entity);
    }

    @Override
    public void merge(final String userId, final String newName, final Project entityToMerge) {
        if (userId.isEmpty() || newName.isEmpty() || entityToMerge == null) return;
        projectRepository.merge(userId, newName, entityToMerge);
    }

    @Override
    public void remove(final String userId, final Project entity) {
        if (userId.isEmpty() || entity == null) return;
        projectRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(final String userId) {
        if (userId.isEmpty()) return;
        projectRepository.removeAll(userId);
    }

}
