package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import com.dolnikova.tm.repository.AbstractRepository;
import com.dolnikova.tm.repository.TaskRepository;

import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService() {
    }

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final String userId) {
        if (userId.isEmpty()) return null;
        return taskRepository.findAll(userId);
    }

    @Override
    public Task findOne(final String userId, final String id) {
        if (userId.isEmpty() || taskRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return taskRepository.findOne(userId, id);
    }

    public Task findOne(final String login) {
        return null;
    }

    @Override
    public void persist(final String userId, final Task entity) {
        if (userId.isEmpty() || entity == null) return;
        taskRepository.persist(userId, entity);
    }

    @Override
    public void merge(final String userId, final String newName, final Task entityToMerge) {
        if (userId.isEmpty() || newName.isEmpty() || entityToMerge == null) return;
        taskRepository.merge(userId, newName, entityToMerge);
    }

    @Override
    public void remove(final String userId, final Task entity) {
        if (userId.isEmpty() || entity == null) return;
        taskRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(final String userId) {
        if (userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    @Override
    public List<Task> getTasksByProjectId(final String userId, final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return null;
        return taskRepository.getTasksByProjectId(userId, projectId);
    }

}
